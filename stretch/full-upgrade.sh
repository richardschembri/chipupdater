#!/bin/sh

sudo cp -i sources.list /etc/apt/sources.list
sudo apt update && sudo apt full-upgrade
echo "Updating NetworkManager configuration"
sudo cp -i NetworkManager.conf /etc/NetworkManager/NetworkManager.conf
echo "Updating xorg configuration"
sudo cp -i xorg.conf /etc/X11/xorg.conf
echo "Updating AwesomeWM configuration"
sudo cp -i rc.lua ~/.config/awesome/rc.lua
