#!/bin/bash

#-------- Helper Functions {{{
#------------------------------------------------------
function installprompt () {
	if command -v ${1} >/dev/null 2>&1; then
		# 1 = false
		return 1
	fi
	read -r -p "Do you want to install ${1}? [y/N] " response
	case "$response" in
		[yY][eE][sS]|[yY]) 
#			0 = true
			return 0
		;;

		[nN][oO]|[nN]) 
#			1 = false
			return 1
		;;
		*)
			return $(installprompt ${1})
		;;
	esac
}


! command -v git >/dev/null 2>&1 && sudo apt-get install git

SOURCEPATH="$HOME/AppSources"

mkdir -p -- $SOURCEPATH

#-------- Rofi 1.7.4 setup {{{
#------------------------------------------------------
if installprompt "rofi"
then
	cd $SOURCEPATH
	# Rofi setup
	SOURCEPATH_ROFI="$SOURCEPATH/rofi"
	[ ! -d $SOURCEPATH_ROFI ] && git clone -b 1.7.4 https://github.com/davatorium/rofi.git && cd $SOURCEPATH_ROFI && git submodule update --init

	sudo apt-get install gcc make autoconf automake pkg-config bison check libpango1.0-dev libpangocairo-1.0-0 libglib2.0-0 libgdk-pixbuf-2.0-dev libstartup-notification0-dev libxkbcommon-dev libxkbcommon-x11-dev libxcb1 xcb-proto libxcb-cursor-dev libxcb-randr0-dev libxcb-xinerama0-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-util-dev

	cd $SOURCEPATH_ROFI
	autoreconf -i
	mkdir -p -- build
	cd build && ../configure && sudo make install
fi
#}}} Rofi 


#-------- NeoVim 0.8.0 setup {{{
#------------------------------------------------------
# sudo apt-get install pkexec acpi powertop
if installprompt "nvim"
then
	SOURCEPATH_NVIM="$SOURCEPATH/neovim"
	cd $SOURCEPATH
	[ ! -d $SOURCEPATH_NVIM ] && git clone -b release-0.9 https://github.com/neovim/neovim.git  
	sudo apt-get install ninja-build gettext libtool libtool-bin autoconf cmake g++ pkg-config unzip curl doxygen

	cd $SOURCEPATH_NVIM && sudo make CMAKE_BUILD_TYPE=RelWithDebInfo && sudo make install
fi

PACKERPATH="$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim"
[ ! -d  $PACKERPATH ] && git clone --depth 1 https://github.com/wbthomason/packer.nvim.git $PACKERPATH
#}}} NeoVim

#-------- st (Simple Temple) {{{
#------------------------------------------------------
if installprompt "st"
then
	SOURCEPATH_ST="$SOURCEPATH/st"
	cd $SOURCEPATH
	git clone https://github.com/LukeSmithxyz/st && cd $SOURCEPATH_ST && sudo make install
fi
#}}} st

#-------- Improved Battery Indicator {{{
#------------------------------------------------------

SOURCEPATH_BATT="$SOURCEPATH/pocketchip-batt"
cd $SOURCEPATH
[ ! -d $SOURCEPATH_BATT ] && \
	git clone https://github.com/aleh/pocketchip-batt.git && \
	sudo apt-get install libx11-dev libxtst-dev && \
	cd $SOURCEPATH_BATT && sudo make install

#}}} Improved Battery Indicator

#-------- glow (Terminal markdown viewer) {{{
#------------------------------------------------------
if installprompt "glow"
then
	sudo mkdir -p /etc/apt/keyrings
	curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
	echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
	sudo apt update && sudo apt install glow
fi
#}}} glow 


#-------- pocket home bismuth (Improved Pocket Home) {{{
#------------------------------------------------------
if installprompt "pocket home bismuth"
then
	SOURCEPATH_POCKETHOME="$SOURCEPATH/pocket-home-bismuth"
	mkdir $SOURCEPATH_POCKETHOME
	cd $SOURCEPATH_POCKETHOME
	wget https://github.com/centuryglass/Pocket-Home-Bismuth/releases/download/v0.1.1/pocket-home_0.1.1_armhf.deb
	sudo dpkg -i pocket-home_0.1.1_armhf.deb
fi
#}}} pocket home bismuth 
