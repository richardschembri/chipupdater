#!/bin/bash

#-------- Colors {{{
#------------------------------------------------------
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
cyan=`tput setaf 6`;
reset=`tput sgr0`
#}}}

declare -A upgradeto
upgradeto[jessie]="stretch"
upgradeto[stretch]="buster"
upgradeto[buster]="bullseye"
upgradeto[bullseye]="bookworm"

#-------- Tools {{{
#------------------------------------------------------------------------------
performfullupgrade(){
	echo "${yellow}Currently on $1 upgrading to ${upgradeto[$1]}${reset}"
	cd ${upgradeto[$1]}
	./full-upgrade.sh
	cd ..
}
#}}}

#------------------------------------------------------------------------------

DEBIAN_VERSION=$(cat /etc/os-release | grep "VERSION=" | cut -d "(" -f2 | cut -d ")" -f1)

case $DEBIAN_VERSION in
	jessie)
		if [ -z $(cat /etc/apt/sources.list | grep "jfpossibilities") ]
		then
			echo "${yellow}Updating jessie sourcelist and performing upgrade${reset}"
			cd jessie
			./upgrade.sh
			cd ..
		else
			performfullupgrade $DEBIAN_VERSION 
		fi
		;;
	stretch|buster|bullseye)
		performfullupgrade $DEBIAN_VERSION 
		;;
	*)
		echo "${green}No upgrade path from $DEBIAN_VERSION${reset}"
		;;
esac

#------------------------------------------------------------------------------
